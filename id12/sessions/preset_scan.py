from bliss.scanning.chain import ChainPreset


class XDL(ChainPreset):

    XDLOPIOM_TIMERES   = 8 ## microseconds (1/125kHz in OPIOM)
    XDLOPIOM_TGMAX     = ((1<<10) - 1) * XDLOPIOM_TIMERES
    XDLOPIOM_TDMAX     = ((1<<9 ) - 1) * XDLOPIOM_TIMERES
    XDLOPIOM_NCYCMAX   = ((1<<16) - 1)
    XDLOPIOM_WRBIT     = 1
    XDLOPIOM_T1BIT     = 2
    XDLOPIOM_T2BIT     = 3
    XDLOPIOM_STARTBIT  = 4
    XDLOPIOM_RESETBIT  = 7
    XDLOPIOM_CHOP_APER = 7462 ## microseconds (1/67Hz)/2
    XDLOPIOM_CHOP_CLOS = 7462 ## microseconds (1/67Hz)/2
    
    def __init__(self, opiom):
        self.opiom = opiom
        self.opiom_name = opiom.name

    def prepare(self, chain):
        """
        Called on the preparation phase of the chain iteration.
        """
        for soft_timer in chain.nodes_list:
            try:
                count_time = soft_timer.count_time
                break
            except AttributeError:
                pass
        else:
            raise RuntimeError(
                "Couldn't guess the count time for the opiom"
            )
        # A chopper cycle (1/67Hz) corresponds to one aperture plus one closure of the chopper.
        nb_chopper_cyc =  (count_time * 67) -1
        self._xdlopiom_reg("NCYC", nb_chopper_cyc) 

    def opiom_setup(self, td, tg):
      
      """ Configure the opiom for using the NECST
             - delay of 1.5 ms at aperture/closure of the chopper"
             - integration of 5 ms during aperture and 5 ms during closure"

             <--aperture--> <---closure--->                            "
              _____________                 _____________              "
             |             |               |             |             "
             |             |               |             |             "
      _______|             |_______________|             |____________ "
                                                                       "
             |<>|<--->|    |<>|<--->|                                  "
              td  tg        td  tg                                     "
      """
      
      XDLOPIOM_TD  =  td * 1000
      XDLOPIOM_TG  =  tg * 1000
      td_opiom = int(XDLOPIOM_TD / self.XDLOPIOM_TIMERES)
      tg_opiom = int(XDLOPIOM_TG / self.XDLOPIOM_TIMERES)
      # XDLOPIOM_TGREAL = tg * self.XDLOPIOM_TIMERES
      err = 0
      if XDLOPIOM_TG < 0 or XDLOPIOM_TG > self.XDLOPIOM_TGMAX:
         print("TG value out of range\n")
         err = 1

      if XDLOPIOM_TD < 0 or XDLOPIOM_TD > self.XDLOPIOM_TDMAX:
         print("TD value out of range\n")
         err = 1
      
      if XDLOPIOM_TG + XDLOPIOM_TD > self.XDLOPIOM_CHOP_APER:
         print("TG + TD is longer than chopper apperture time\n")
         err = 1
      
      if XDLOPIOM_TG + XDLOPIOM_TD > self.XDLOPIOM_CHOP_CLOS:
         print("TG + TD is longer than chopper closure time\n")
         err = 1
      
      if err == 0:
         self._xdlopiom_reg("TD", td_opiom)
         self._xdlopiom_reg("TG", tg_opiom)
         print("\n %s configured with a resolution of values of 8 us.\n" % self.opiom_name)
         print("\tDelay time set to TD = %.1f ms\n" % td)
         print("\tGate time set to TG = %.1f ms\n"  % tg)
      else:
          print("\n\t ATTENTION.The NECST is not usable.\n\n")
      
    def _xdlopiom_reg(self, reg, val):
        if reg == "TG":
            self._xdlopiom_timereg(0, 0)
            self._xdlopiom_val_write(val)
        elif reg == "TD":
            self._xdlopiom_timereg(0, 1)
            self._xdlopiom_val_write(val)
        elif reg == "NCYC":
           self._xdlopiom_timereg(1, 1)
           self._xdlopiom_val_write(val)
        else:
            print("Non valid register\n")
                      
        self.opiom.comm("SPBIT %d 0" %  self.XDLOPIOM_WRBIT)
        self.opiom.comm("SPBIT %d 1" %  self.XDLOPIOM_WRBIT)
        self.opiom.comm("SPBIT %d 0" %  self.XDLOPIOM_WRBIT)

    def _xdlopiom_timereg(self, v1, v2):
        self.opiom.comm("SPBIT %d %d" % ( self.XDLOPIOM_T1BIT, v1))
        self.opiom.comm("SPBIT %d %d" % ( self.XDLOPIOM_T2BIT, v2))

    def _xdlopiom_val_write(self, val):
        val = int(val)
        lsb = val & 0xFF
        msb = val >> 8

        self.opiom.comm("IM %s"  % hex(lsb))
        self.opiom.comm("IMA %s" % hex(msb))
   




      

# 78.ID12> prdef xdlopiom_reg

# # /users/blissadm/local/spec/macros/id12xdl.mac
# def xdlopiom_reg(dev, reg, val) '{
#    local comm
#    if (reg == "TG") {
#       _xdlopiom_timereg(dev, 0, 0)
#       _xdlopiom_val_write(dev, val)
#    } else if (reg == "TD") {
#       _xdlopiom_timereg(dev, 0, 1)
#       _xdlopiom_val_write(dev, val)
#    } else if (reg == "NCYC") {
#       _xdlopiom_timereg(dev, 1, 1)
#       _xdlopiom_val_write(dev, val)
#    } else {
#       print "Non valid register"
#    }
#    comm = sprintf("SPBIT %d 0", XDLOPIOM_WRBIT)
#    isgdevice_comm_ack(dev, comm)
#    comm = sprintf("SPBIT %d 1", XDLOPIOM_WRBIT)
#    isgdevice_comm_ack(dev, comm)
#    comm = sprintf("SPBIT %d 0", XDLOPIOM_WRBIT)
#    isgdevice_comm_ack(dev, comm)
# }'

# 879.ID12> prdef  _xdlopiom_timereg

# # /users/blissadm/local/spec/macros/id12xdl.mac
# def _xdlopiom_timereg(dev, v1, v2) '{
#    local comm
#    comm = sprintf("SPBIT %d %d", XDLOPIOM_T1BIT, v1)
#    isgdevice_comm_ack(dev, comm)
#    comm = sprintf("SPBIT %d %d", XDLOPIOM_T2BIT, v2)
#    isgdevice_comm_ack(dev, comm)
#    return(0)
# }'

# 880.ID12> prdef  _xdlopiom_val_write

# # /users/blissadm/local/spec/macros/id12xdl.mac
# def _xdlopiom_val_write(dev, val) '{
#    local comm msb lsb

#    lsb = val & 0xFF
#    msb = val >> 8
#    comm = sprintf("IM 0x%X", lsb)
#    isgdevice_comm_ack(dev, comm) 
#    comm = sprintf("IMA 0x%X", msb)
#    isgdevice_comm_ack(dev, comm)
#    if (XDLOPIOM_DEBUG == 1) {
#       printf("LSB = 0x%X\n", lsb)
#       printf("MSB = 0x%X\n", msb)
#    }
#    return(0)
# }'

============
ID12 project
============

[![build status](https://gitlab.esrf.fr/ID12/id12/badges/master/build.svg)](http://ID12.gitlab-pages.esrf.fr/ID12)
[![coverage report](https://gitlab.esrf.fr/ID12/id12/badges/master/coverage.svg)](http://ID12.gitlab-pages.esrf.fr/id12/htmlcov)

ID12 software & configuration

Latest documentation from master can be found [here](http://ID12.gitlab-pages.esrf.fr/id12)
